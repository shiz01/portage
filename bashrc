

# CCACHE:
#

#if [[ ${FEATURES} == *ccache* && ${EBUILD_PHASE_FUNC} == src_* ]]; then
#    export CCACHE_DIR=${CCACHE_DIR}/${CATEGORY}/${PN}
#    mkdir -p "${CCACHE_DIR}" || die 
#fi



#pre_src_prepare() {
#    [[ ${EAPI:-0} == [012345] ]] || return
#    if ! type epatch_user > /dev/null 2>&1; then
#        local names="EPATCH_USER_SOURCE epatch_user epatch evar_push evar_push_set evar_pop estack_push estack_pop"
#        source <(awk "/^# @(FUNCTION|VARIABLE): / { p = 0 } /^# @(FUNCTION|VARIABLE): (${names// /|})\$/ { p = 1 } p { print }" ${PORTDIR}/eclass/eutils.eclass)
#    fi
# 
#    epatch_user
# 
#    for name in $names; do
#        unset $name
#    done
#}

# 
#PROFILE_DIR="${PGO_DIR}/${CATEGORY}/${P}"
# 
#CFLAGS_PROFILE_GEN="-fprofile-generate=${PROFILE_DIR} -fprofile-arcs -fvpt -pg"
#CFLAGS_PROFILE_USE="-fprofile-use=${PROFILE_DIR} -fprofile-correction"
# 
#CXXFLAGS_PROFILE_GEN="-fprofile-generate=${PROFILE_DIR} -fprofile-arcs -fvpt -pg"
#CXXFLAGS_PROFILE_USE="-fprofile-use=${PROFILE_DIR} -fprofile-correction"
# 
#LDFLAGS_PROFILE_GEN="-fprofile-arcs -pg"
# 
# 
#if [ "${EBUILD_PHASE}" == "unpack" ]
#then
#    if [[ "${PGO_ENABLE^^}" == "X" ]] ; then
#        
#        if [[ "$CFLAGS" != *"gdb"* || "$CXXFLAGS" != *"gdb"* || "${USE}" != *"pgo"* ]]
#        then
#                elog "Portage will try to build ${CATEGORY}/${PN} with PGO."
#                mkdir -p "${PROFILE_DIR}" || die
#                if [[ -n "$(ls -A "${PROFILE_DIR}")" ]]
#                then
#                    elog "Portage will use the analysis that is stored at ${PROFILE_DIR}."
#                    CFLAGS="${CFLAGS}  ${CFLAGS_PROFILE_USE}" 
#                    CXXFLAGS="${CXXFLAGS}  ${CXXFLAGS_PROFILE_USE}"
#                    LDFLAGS="${LDFLAGS} " 
#                else                    
#                    elog "Running with PGO for the first time. Use ${P} before you recompile it."
#                    CFLAGS="${CFLAGS} ${CFLAGS_PROFILE_GEN}"
#                    CXXFLAGS="${CXXFLAGS} ${CXXFLAGS_PROFILE_GEN}"
#                    LDFLAGS="${LDFLAGS}  ${LDFLAGS_PROFILE_GEN}" 
#                fi
#        else
#            if [[ "$IUSE" == "*pgo*" ]] ; then
#                elog "PGO is in USE flags, no need to use this bashrc."
#            fi
#        fi
#    fi
# 
#fi
# 
# 
#if [ "${EBUILD_PHASE}" == "postinst" ]
#then
#    if [[ ${CFLAGS} == "*-fprofile-generate*"  ]]  ; then
#        for i in $(qlist -o "${PN}" | grep -e ".so" -e "/bin/")
#        do
#            if [[ -z $(strings "${i}" | grep profiling) ]]; then
#                ewarn "${i} isn't making use of PGO while such cflags were applied to ${CATEGORY}/${P}"
#            else
#                elog  "${i} is making use of PGO"
#            fi
#        done
#    fi
#    elog "Calling updatedb to update its database."
#    updatedb
# 
#    chmod  777 -R  "${PGO_DIR}"
#fi

