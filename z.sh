sleep_time="2m"

cmd="nice -n 19 ionice -c2 -n7 ${@}"

while true;
do
        is_wait=$(ps aux | grep emerge | grep -v "grep" | wc -l);
        if [[ $is_wait == 0 ]]; then
                echo "$(date +"%Y-%m-%dT%H-%M-%S")";
                echo "Execute command: ${cmd}" && ${cmd} && exit 0;
                exit -1;
        else
            echo -e "$(date +'%Y-%m-%dT%H-%M-%S'): Emerge procces: ${is_wait}. Sleep ${sleep_time} and retry."
            echo -e "$(date +'%Y-%m-%dT%H-%M-%S'): wait to execute command: ${@}"
        fi
        sleep "${sleep_time}";
done;


